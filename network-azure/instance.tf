locals {
  tag_map = merge(
    var.custom_tags,
    {
      application_type   = var.application_type
      cloud_type         = var.cloud_type
      co_id              = var.co_id
      name               = var.network_name
      resource_type      = var.resource_type
      subscription       = var.subscription
      subscription_group = var.subscription_group
      wallet_id          = var.wallet_id
      workspace_id       = var.workspace_id
    }
  )
}

resource "azurerm_resource_group" "azure_resource_group" {
  name     = "rsc-${var.workspace_id}"
  location = var.azure_location
  tags     = local.tag_map
}

resource "azurerm_virtual_network" "avn" {
  name                = "${azurerm_resource_group.azure_resource_group.name}_avn"
  address_space       = [var.cidr]
  location            = azurerm_resource_group.azure_resource_group.location
  resource_group_name = azurerm_resource_group.azure_resource_group.name
  tags = merge(
    local.tag_map,
    {
      res_type_subscription = "${var.resource_type} - ${var.subscription}"
    }
  )
}

resource "azurerm_subnet" "subnet" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.azure_resource_group.name
  virtual_network_name = azurerm_virtual_network.avn.name
  address_prefixes     = [var.cidr]
}
