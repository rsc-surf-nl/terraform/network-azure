# Resource specific parameters

variable "network_name" {
  type        = string
  description = "The name of this network"
  default     = "rsc-network"
}

variable "description" {}

variable "cidr" {
  type        = string
  description = "The CIDR network space to use for the local (Azure) network of the VM"
  default     = "10.0.0.0/16"
  validation {
    condition     = length(regexall("^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})$", var.cidr)) > 0
    error_message = "The `cidr` should be a valid CIDR network space in a notation like '10.0.0.0/16'."
  }
}

variable "custom_tags" {
  description = "Additional tags to attach to the resources"
  type        = map(any)
  default     = {}
}

# Azure specific variables

variable "azure_subscription_id" {
  type        = string
  description = "[subscription] The Azure subscription under which to start this VM"
}

variable "azure_client_id" {
  type        = string
  description = "[subscription] The client ID that should be used for provisioning the resources"
}

variable "azure_client_secret" {
  type        = string
  description = "[subscription] The client secret that should be used for provisioning the resources"
}

variable "azure_tenant_id" {
  type        = string
  description = "[subscription] The ID of the tenant to which the client ID belongs"
}

variable "azure_location" {
  type        = string
  description = "[subscription] The Azure location where to deploy the VM"
  default     = "West Europe"
}

# SRC automatic variables are added explicitly to be self-contained
variable "application_type" {
  type        = string
  description = "The application type (eg. 'Compute', 'Storage' etc.)"
  default     = "Network"
}
variable "cloud_type" {
  type        = string
  description = "Cloud type (eg. 'AWS', 'Openstack', 'Azure' etc.)"
  default     = "Azure"
}
variable "co_id" {
  type        = string
  description = "The ID of the associated CO"
  default     = "unset"
}
variable "resource_type" {
  type        = string
  description = "The resource type (eg. 'VM', 'Storage-Volume' etc.)"
  default     = "Private-Network"
}
variable "subscription" {
  type        = string
  description = "The (SRC) subscription under which this resources is created"
  default     = "unset"
}
variable "subscription_group" {
  type        = string
  description = "The subscription group to which the `subscription` belongs"
  default     = "unset"
}
variable "wallet_id" {
  type        = string
  description = "The ID of the associated wallet"
  default     = "unset"
}
variable "workspace_fqdn" {
  type        = string
  description = "The FQDN assigned to this workspace"
  default     = "example.surf-hosted.nl"
}
variable "workspace_id" {
  type        = string
  description = "The ID of this workspace"
  default     = "unset"
}
