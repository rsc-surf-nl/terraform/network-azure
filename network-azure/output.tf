output "id" {
  value = azurerm_virtual_network.avn.id
}

output "network_id" {
  value = azurerm_subnet.subnet.id
}

output "network_name" {
  value = var.network_name
}

output "subnet_id" {
  value = azurerm_subnet.subnet.id
}

output "subnet_cidr" {
  value = azurerm_subnet.subnet.address_prefixes
}

output "description" {
  value = var.description
}
